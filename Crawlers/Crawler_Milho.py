from bs4 import BeautifulSoup
from lxml import html
import requests
import pymongo
import datetime

conexao = pymongo.MongoClient("mongodb+srv://user123:user123@cluster-cpivz.mongodb.net/test?retryWrites=true")
mydb = conexao['Cotacao']

date = datetime.date(2017,1,1)
today = datetime.date.today()
oneday = datetime.timedelta(days = 1)
date_counter = 0

def genDates(someDate):
        while someDate != today:
                someDate += oneday
                yield someDate
                
for d in genDates(date):
        date_counter += 1
        dia = int (d.day)
        mes = int (d.month)
        ano = int (d.year)
        if mes <10:
                string_mes = str(mes)
                mes_concatenado = '0'+string_mes
        else:
                mes_concatenado=str(mes)
        if dia <10: 
                string_dia= str(dia)
                dia_concatenado = '0'+string_dia
        else:
                dia_concatenado=str(dia)
     
        link = str(f'https://www.noticiasagricolas.com.br/cotacoes/milho/milho-mercado-fisico-sindicatos-e-cooperativas/{ano}-{mes_concatenado}-{dia_concatenado}')
        url = requests.get(link)
        soup = BeautifulSoup(url.content, 'html.parser')
        preco = soup.select('table.cot-fisicas tbody tr')
        data = soup.select('div.info')

        for x in data:
                for i in preco:
                        item = i.text
                        informacoes = str(item).strip().split('\n')
                        databr = str(f"{dia_concatenado}/{mes_concatenado}/{ano}")
                        dataAtual = str(databr).split('/')[::-1]
                        dataAtual = str(dataAtual[0] + '-' + dataAtual[1] + '-' + dataAtual[2])
                        dataAtual = datetime.datetime.strptime(dataAtual, "%Y-%m-%d")
                        if len(informacoes) < 3:
                                informacoes.extend(['0,0'])
                        if (informacoes[1].startswith("S") or informacoes[1].startswith("s") or informacoes[1].endswith("-") or informacoes[1].startswith("F")):
                                informacoes[1]="0,0"

                        if (informacoes[2].endswith("-") or informacoes[2].startswith("F") or informacoes[2].startswith("")):
                                informacoes[2]="0,0"
                        confirma_valor = float(informacoes[1].split('(')[0].replace(',', '.'))
                        if (confirma_valor > 0):                               
                                print(f"Data: {dataAtual} || Cidade: {informacoes[0]} || Preço: {informacoes[1]} || Variação: {informacoes[2]}")
                                
                                mydb.Milho.update(
                                        {
                                                "data" : dataAtual,
                                                "cidade" : informacoes[0].split('(')[0],
                                                "valor" : float(informacoes[1].split('(')[0].replace(',', '.')),
                                                "variante" : float(informacoes[2].replace(',', '.'))
                                        }, 
                                        {
                                                "data" : dataAtual,
                                                "cidade" : informacoes[0].split('(')[0],
                                                "valor" : float(informacoes[1].split('(')[0].replace(',', '.')),
                                                "variante" : float(informacoes[2].replace(',', '.'))
                                        },
                                        upsert = True
                               )
