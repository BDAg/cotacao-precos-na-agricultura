from bs4 import BeautifulSoup
from lxml import html
import requests
import pymongo
import datetime


conexao = pymongo.MongoClient("mongodb+srv://user123:user123@cluster-cpivz.mongodb.net/test?retryWrites=true")
mydb = conexao['Cotacao']

date = datetime.date(2017,1,16)##Data Histórica para iniciar 2010,11,17
today = datetime.date.today()##Dia Atual
oneday = datetime.timedelta(days = 1)

date_counter = 0
def genDates(someDate):
        while someDate != today:
                someDate += oneday
                yield someDate
for d in genDates(date):
        date_counter += 1
        dia = int (d.day)
        mes = int (d.month)
        ano = int (d.year)
        if mes <10:
                smes = str(mes)
                cmes = '0'+smes
        else:
                cmes=str(mes)
        if dia <10:
                sdia= str(dia)
                cdia = '0'+sdia
        else:
                cdia=str(dia)
        textoano = str('https://www.noticiasagricolas.com.br/cotacoes/sorgo/sorgo-mercado-fisico-media/%d' %ano )
        textomes = str(textoano +'-%s' %cmes)
        textodia = str(textomes +'-%s'%cdia)
        link = str(textodia)
        url = requests.get(link)
        soup = BeautifulSoup(url.content, 'html.parser')
        preco = soup.select('table.cot-fisicas tbody tr')
        data = soup.select('div.info')
        for x in data:
                for i in preco:
                        item = i.text
                        item2 = str(item).strip().split('\n')

                        databr = str("{}/{}/{}".format(cdia,cmes,ano))
                        dataAtual = str(databr).split('/')[::-1]
                        dataAtual = str(dataAtual[0] + '-' + dataAtual[1] + '-' + dataAtual[2])
                        dataAtual = datetime.datetime.strptime(dataAtual, "%Y-%m-%d")
                        print(list(item2))
                        if (item2[1].startswith("s/") or item2[1].startswith("s") or item2[1].endswith("-") or item2[1].startswith("F")):
                                item2[1]="0,0"

                        if (item2[2].endswith("-") or item2[2].startswith("F")):
                                item2[2]="0,0"

                        
                        print("Data: {} || Cidade: {} || Preço: {} || Variação: {}".format(dataAtual,item2[0],item2[1],item2[2]))
                        
                        mydb.Sorgo.update(
                                {
                                        "data" : dataAtual,
                                        "cidade" : item2[0],
                                        "valor" : float(item2[1].split('/')[0].replace(',', '.')),
                                        "variacao" : float(item2[2].replace(',', '.'))
                                },
                                {
                                        "data" : dataAtual,
                                        "cidade" : item2[0],
                                        "valor" : float(item2[1].split('/')[0].replace(',', '.')),
                                        "variacao" : float(item2[2].replace(',', '.')),
                                        "unidade": 'Saca 60Kg'
                                },
                                upsert = True
                        )