from bs4 import BeautifulSoup
from lxml import html
import requests
import pymongo
import datetime

conexao = pymongo.MongoClient("mongodb+srv://user:user@cluster-cpivz.mongodb.net/test?retryWrites=true")
mydb = conexao['Cotacao']

request= requests.get('https://www.noticiasagricolas.com.br/cotacoes/amendoim/amendoim-ceasas')

soup=BeautifulSoup(request.content, 'html.parser')

tabela = soup.select('table.cot-fisicas tbody tr')
data = soup.select('div.cotacao div.info div.fechamento')

tabela.pop(0)
for i in data:
    for x in tabela:
        data = i.text
        data = data.split()

        item = x.text
        item2 = str(item).strip().split('\n')
        
        dataAtual = str(data[1]).split('/')[::-1]
        dataAtual = str(dataAtual[0] + '-' + dataAtual[1] + '-' + dataAtual[2])
        dataAtual = datetime.datetime.strptime(dataAtual, "%Y-%m-%d")


        print ('Data : {} || Dados : {}'. format(data[1],item2))

        mydb.Amendoim.update(
                            {
                                "data" : dataAtual,
                                "praça" : item2[0],
                                "Preço" : (item2[1].replace(',', '.')),
                                "variação" : (item2[2].replace(',', '.'))
                            },
                            {
                                "data" : dataAtual,
                                "praça" : item2[0],
                                "Preço" : (item2[1].replace(',', '.')),
                                "variação" : (item2[2].replace(',', '.'))
                            },
                            upsert = True
                        )
