from bs4 import BeautifulSoup
from lxml import html
import requests
import pymongo
import datetime

conexao = pymongo.MongoClient("mongodb+srv://user123:user123@cluster-cpivz.mongodb.net/test?retryWrites=true")
mydb = conexao['Cotacao']

date = datetime.date(2010,11,1)##Data Histórica para iniciar 2010,11,17
today = datetime.date.today()##Dia Atual
oneday = datetime.timedelta(days = 1)

cidades=list()

date_counter = 0
def genDates(someDate):
        while someDate != today:
                someDate += oneday
                yield someDate
for d in genDates(date):
        date_counter += 1
        dia = int (d.day)
        mes = int (d.month)
        ano = int (d.year)
        if mes <10:
                smes = str(mes)
                cmes = '0'+smes
        else:
                cmes=str(mes)
        if dia <10:
                sdia= str(dia)
                cdia = '0'+sdia
        else:
                cdia=str(dia)
        textoano = str('https://www.noticiasagricolas.com.br/cotacoes/cafe/cafe-arabica-mercado-fisico-tipo-45/%d' %ano )
        textomes = str(textoano +'-%s' %cmes)
        link = str(textomes +'-%s'%cdia)
        url = requests.get(link)
        soup = BeautifulSoup(url.content, 'html.parser')
        tabela = soup.select('table.cot-fisicas tbody tr')
        data = soup.select('div.info')

        for x in data:
                for i in tabela:
                        item = i.text
                        item2 = str(item).strip().split('\n')
                        
                        databr = str("{}/{}/{}".format(cdia,cmes,ano))
                        dataAtual = str(databr).split('/')[::-1]
                        dataAtual = str(dataAtual[0] + '-' + dataAtual[1] + '-' + dataAtual[2])
                        dataAtual = datetime.datetime.strptime(dataAtual, "%Y-%m-%d")

                        preco= item2[1]
                        if (preco.startswith("s/") or item2[1].endswith("-") or item2[1].startswith("F")):
                                preco="0,0"

                        variacao= item2[2]
                        if (variacao.endswith("-") or item2[2].startswith("F")):
                                variacao="0,0"

                        cidade=item2[0].strip()
                        if (len(cidades)<1):
                                cidades.append(cidade)
                        if (cidade not in cidades):
                                cidades.append(cidade)
                        for i in range(len(cidades)):
                                if (cidade[0:4]==cidades[i][0:4]):
                                        cidade=cidades[i]
                                        break

                        estado=cidade[len(cidade)-2:len(cidade)+1]  

                        preco= float(preco.split()[0].replace(',', '.'))
                        variacao= float(variacao.replace(',', '.'))

                        if (preco>0):
                                print(f"Data: {dataAtual} || Cidade: {cidade} ||Estado: {estado}|| Preço: {preco} || Variação: {variacao}")
                                
                                mydb.Cafe.update(
                                        {
                                                "data" : dataAtual,
                                                "cidade" : cidade,
                                                "preco" : preco,
                                                "variacao" : variacao
                                        },
                                        {
                                                "data" : dataAtual,
                                                "cidade" : cidade,
                                                "preco" : preco,
                                                "variacao" : variacao,
                                                "estado": estado
                                        },
                                        upsert = True
                                )
                