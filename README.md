# Cotaçao de preços na agricultura

O projeto consiste em coletar dados (cotação de preços), de diversas culturas e armazenando-os em um banco de dados. Para depois utilizar o meta base para mostrar os dados em gráfico, alem de comparar os dados.
<br><br>
# Documentação
<br>[Matriz de Habilidades](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Matriz-de-Habilidade)</br>
<br>[Matriz de Habilidades (Fim do projeto)](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Matriz-de-Habilidades-(Fim-do-projeto))</br>
<br>[Mapa de Conhecimento](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Mapa-de-Conhecimento)</br>
<br>[Cronograma](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Cronograma)</br>
<br>[Project Charter](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Project-Charter)</br>
<br>[Lições Aprendidas](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Li%C3%A7%C3%B5es-Aprendidas)</br>

# Equipe
<br>[Mateus Teodoro (Scrum Master)](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Equipe/Mateus-Teodoro)</br>
<br>[Denise Boito Pereira da Silva](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Equipe/Denise-Boito-Pereira-da-Silva)</br>
<br>[Gabriel Lima](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Equipe/Gabriel-Lima)</br>
<br>[Gabriel Luiz Oliveira da Costa ](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Equipe/Gabriel-Luiz-Oliveira-da-Costa)</br>
<br>[Iago Medeiros](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Equipe/Iago-Medeiros)</br>
<br>[João Vitor Prates Alves](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Equipe/Jo%C3%A3o-Vitor-Prates-Alves)</br>
<br>[Maicon Aparecido da Cruz Lopes](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Equipe/Maicon-Aparecido-da-Cruz-Lopes)</br>
<br>[Vagner Henrique Ferraz](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Equipe/Vagner-Henrique-Ferraz)</br>
<br>[Victor Shin Iti Omae Nakamura](https://gitlab.com/BDAg/cotacao-precos-na-agricultura/wikis/Equipe/Victor-Shin-Iti-Omae-Nakamura)</br>

# Como utilizar o Crawler

Para rodar o crawler em seu computador é necessário configurar o ambiente com algumas bibliotecas, afim de garantir um funcionamento 100% satisfatório.
As configurações a seguir foram executadas em Ubuntu 18.04 LTS, e seguem o mesmo padrão para outras distribuições baseadas em Debian ou no próprio Ubuntu.

**Python** 

O código está estruturado em Python 3, que já vem instalado na versão 3.6.7 por padrão no Ubuntu 18.04 LTS. Para verificar se o Python 3 encontra-se instalado em seu computador, basta abrir um novo terminal (Control+Shift+T no teclado) e digitar:


```bash
python3 --version

```

O terminal também pode ser utilizado para rodar o crawler após a instalação das bibliotecas, como será detalhado no final.

**MongoDB**

As informações coletadas pelo crawler precisam ser armazenadas para que possam ser posteriormente utilizadas e analisadas. Para tanto, utilizamos o Banco de Dados Não Relacional (NOSql) MongoDB. Para criar uma conta online e inserir seus dados basta acessar a página: https://www.mongodb.com/

**Bibliotecas**

Como mencionado anteriormente, nosso código roda em Python 3 e para isso utilizaremos seu gerenciador de pacotes, o PIP. 
para instalar o PIP no Ubuntu, basta abrir o terminal e digitar:


```bash
sudo apt-get install python3-pip
```

para saber se a instalação ocorreu com sucesso, após os pacotes baixados basta inserir o comando:

```bash
pip3 --version
```

O resultado será semelhante a este:

`pip 9.0.1 from /usr/lib/python3/dist-packages (python 3.6)`

Após instalado o PIP, prosseguiremos com a instalação das bibliotecas necessárias. O Python já vem com algumas bibliotecas nativamente instaladas, como a datetime utilizada no projeto e precisaremos apenas daquelas que ainda não estão presentes:


bs4:


```bash
pip3 install bs4
```

requests:

```bash
pip3 install requests
```

lxml:


```bash
pip3 install lxml
```

pymongo:

```bash
pip3 install pymongo
pip3 install dnspython
```

Para verificar se as bibliotecas foram instaladas basta utilizar o comando:


```bash
pip3 list
```

Ao final da instalação das bibliotecas e criação da base de dados no MongoDB basta acessar a pasta em que o crawler se encontra através do terminal e executar o seguinte comando, substituindo a palavra "codigo" pelo nome do arquivo do crawler desejado:


```bash
python3 codigo.py
```



